﻿using CsiMediaTechnicalTest.DataAccess.Models;
using CsiMediaTechnicalTest.Service.Services;
using Moq;

namespace CsiMediaTechnicalTest.Service.UnitTests;

[TestFixture]
public class SortOperationServiceTests
{
    private SortOperationService _service;
    private Mock<IRepository<SortOperation>> _mockRepository;

    [SetUp]
    public void Setup()
    {
        _mockRepository = new Mock<IRepository<SortOperation>>();
        _service = new SortOperationService(_mockRepository.Object);
    }
    
    [Test]
    public async Task SaveOperation_SavesSortOperation_Successfully()
    {
        var sortOperation = new SortOperation
        {
            Id = It.IsAny<int>(),
            SortedNumbers = It.IsAny<string>(),
            SortOrder = It.IsAny<string>(),
            SortTime = It.IsAny<DateTime>(),
            Duration = It.IsAny<TimeSpan>()
        };

        await _service.SaveOperation(sortOperation);

        _mockRepository.Verify(x => x.AddAsync(sortOperation), Times.Once);
        _mockRepository.Verify(x => x.SaveChangesAsync(), Times.Once);
    }
    
    [Test]
    public async Task GetSortOperations_ReturnsAllSortOperations_Successfully()
    {
        var listSortOperation = new List<SortOperation>
        {
            new SortOperation
            {
                Id = It.IsAny<int>(),
                SortedNumbers = It.IsAny<string>(),
                SortOrder = It.IsAny<string>(),
                SortTime = It.IsAny<DateTime>(),
                Duration = It.IsAny<TimeSpan>()
            }
        };
        
        _mockRepository.Setup(x => x.GetAllAsync()).ReturnsAsync(listSortOperation);

        var result = await _service.GetSortOperations();

        Assert.That(result, Is.EqualTo(listSortOperation));
    }
}