﻿using System.Diagnostics;
using System.Text.Json;
using CsiMediaTechnicalTest.DataAccess.Models;
using CsiMediaTechnicalTest.Service.Services;
using Microsoft.AspNetCore.Mvc;

namespace CsiMediaTechnicalTest.Controllers;

public class SortController(ISortOperationService service) : Controller
{
    private static readonly char[] NumberSeparator = [',', ' '];

    private static readonly JsonSerializerOptions ExportJsonOptions = new()
    {
        WriteIndented = true,
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
    };

    public async Task<IActionResult> Index()
    {
        var sortOperations = await service.GetSortOperations();

        ViewBag.SortOperations = sortOperations;

        return View("Index");
    }

    [HttpPost]
    public async Task<IActionResult> SortNumbers(SortInputModel inputModel)
    {
        if (!ModelState.IsValid)
            return View("Index", inputModel);

        try
        {
            var numbers = ParseInputNumbers(inputModel.Numbers);
            SortNumbers(numbers, inputModel.SortOrder);
                
            var operation = CreateSortOperation(numbers, inputModel.SortOrder);

            await service.SaveOperation(operation);
            
            TempData["SuccessMessage"] = "Numbers sorted successfully.";

            return RedirectToAction("Index");
        }
        catch (Exception ex)
        {
            var sortOperations = await service.GetSortOperations();
            ViewBag.SortOperations = sortOperations ?? Enumerable.Empty<SortOperation>();
            
            ModelState.AddModelError("", $"An error occurred: {ex.Message}");
        }

        return View("Index", inputModel);
    }
    
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }


    private static List<int> ParseInputNumbers(string numbers)
    {
        return numbers.Split(NumberSeparator, StringSplitOptions.RemoveEmptyEntries)
            .Select(int.Parse)
            .ToList();
    }

    private static void SortNumbers(List<int> numbers, string sortOrder)
    {
        switch (sortOrder)
        {
            case "Ascending":
                numbers.Sort();
                break;
            case "Descending":
                numbers.Sort((a, b) => b.CompareTo(a));
                break;
        }
    }

    private static SortOperation CreateSortOperation(IEnumerable<int> numbers, string sortOrder)
    {
        var stopwatch = Stopwatch.StartNew();

        var operation = new SortOperation
        {
            SortedNumbers = string.Join(",", numbers),
            SortOrder = sortOrder,
            SortTime = DateTime.Now,
            Duration = stopwatch.Elapsed
        };

        stopwatch.Stop();

        return operation;
    }

    public async Task<IActionResult> ExportToJson()
    {
        var sortOperations = await service.GetSortOperations();

        var json = JsonSerializer.Serialize(sortOperations, ExportJsonOptions);

        const string fileName = "SortOperations.json";
        const string contentType = "application/json";
        var bytes = System.Text.Encoding.UTF8.GetBytes(json);
        
        return File(bytes, contentType, fileName);
    }
}