﻿using System.Text.Json;
using CsiMediaTechnicalTest.Controllers;
using CsiMediaTechnicalTest.DataAccess.Models;
using CsiMediaTechnicalTest.Service.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace CsiMediaTechnicalTest.UnitTests;

[TestFixture]
public class SortControllerTests
{
    private Mock<ISortOperationService> mockService;
    private SortController controller;

    [SetUp]
    public void SetUp()
    {
        mockService = new Mock<ISortOperationService>();
        controller = new SortController(mockService.Object);
    }
    
    [Test]
    public async Task Index_ReturnsViewWithCorrectModel()
    {
        // Arrange
        var operations = new List<SortOperation>
        {
            new SortOperation
            {
                Id = 1, SortedNumbers = "1, 2, 3",
                SortOrder = "Ascending",
                SortTime = DateTime.Now,
                Duration = TimeSpan.FromSeconds(5)
            }
        };
        
        mockService.Setup(s => s.GetSortOperations()).ReturnsAsync(operations);

        // Act
        var result = await controller.Index();

        // Assert
        var viewResult = result as ViewResult;
        
        Assert.That(viewResult, Is.Not.Null);
        Assert.That(viewResult.ViewData["SortOperations"], Is.InstanceOf<List<SortOperation>>());
        
        var model = viewResult.ViewData["SortOperations"] as List<SortOperation>;
        
        CollectionAssert.AreEqual(operations, model);
    }
    
    [Test]
    public async Task SortNumbers_WithInvalidModel_ReturnsViewWithModel()
    {
        // Arrange
        controller.ModelState.AddModelError("Numbers", "Required");
        var inputModel = new SortInputModel();

        // Act
        var result = await controller.SortNumbers(inputModel);

        // Assert
        Assert.That(result, Is.InstanceOf<ViewResult>());
        
        var viewResult = result as ViewResult;
        
        Assert.That(viewResult.Model, Is.EqualTo(inputModel));
    }
    
    [Test]
    public async Task ExportToJson_ReturnsCorrectJson()
    {
        // Arrange
        var operations = new List<SortOperation>
        {
            new SortOperation
            {
                Id = 1,
                SortedNumbers = "1, 2, 3",
                SortOrder = "Ascending",
                SortTime = DateTime.Now,
                Duration = TimeSpan.FromSeconds(5)
            }
        };
        
        mockService.Setup(s => s.GetSortOperations()).ReturnsAsync(operations);

        // Act
        var result = await controller.ExportToJson();

        // Assert
        Assert.That(result, Is.InstanceOf<FileContentResult>());
        var fileResult = result as FileContentResult;
        
        Assert.That(fileResult.ContentType, Is.EqualTo("application/json"));

        // More assertions to verify the content of fileResult.FileContents
        var expectedJson = JsonSerializer.Serialize(operations, new JsonSerializerOptions
        {
            WriteIndented = true, PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });
        
        var expectedBytes = System.Text.Encoding.UTF8.GetBytes(expectedJson);
        
        CollectionAssert.AreEqual(expectedBytes, fileResult.FileContents);
    }
}