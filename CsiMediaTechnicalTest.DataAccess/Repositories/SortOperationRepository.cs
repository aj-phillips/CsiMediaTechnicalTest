﻿using CsiMediaTechnicalTest.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace CsiMediaTechnicalTest.DataAccess.Repositories
{
    public class SortOperationRepository(AppDbContext context) : IRepository<SortOperation>
    {
        public async Task<IEnumerable<SortOperation>> GetAllAsync()
        {
            return await context.SortOperations
                .OrderByDescending(so => so.SortTime)
                .ToListAsync();
        }

        public async Task<SortOperation> GetByIdAsync(int id)
        {
            var entity = await context.SortOperations.FindAsync(id);

            if (entity == null)
            {
                throw new InvalidOperationException($"Unable to find results data with ID: {id}");
            }

            return entity;
        }

        public async Task AddAsync(SortOperation entity)
        {
            await context.SortOperations.AddAsync(entity);
        }

        public void Update(SortOperation entity)
        {
            context.SortOperations.Update(entity);
        }

        public void Delete(SortOperation entity)
        {
            context.SortOperations.Remove(entity);
        }

        public async Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}