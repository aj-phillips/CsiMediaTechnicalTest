﻿namespace CsiMediaTechnicalTest.DataAccess.Models;

public class SortInputModel
{
    public string Numbers { get; set; }
    public string SortOrder { get; set; }
}