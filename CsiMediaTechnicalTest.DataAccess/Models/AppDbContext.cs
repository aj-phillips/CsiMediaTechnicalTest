﻿using Microsoft.EntityFrameworkCore;

namespace CsiMediaTechnicalTest.DataAccess.Models;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public DbSet<SortOperation> SortOperations { get; set; }
}