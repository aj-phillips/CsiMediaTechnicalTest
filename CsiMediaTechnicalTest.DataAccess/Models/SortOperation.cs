﻿namespace CsiMediaTechnicalTest.DataAccess.Models;

public class SortOperation
{
    public int Id { get; set; }
    public string SortedNumbers { get; set; }
    public string SortOrder { get; set; }
    public DateTime SortTime { get; set; }
    public TimeSpan Duration { get; set; }
}