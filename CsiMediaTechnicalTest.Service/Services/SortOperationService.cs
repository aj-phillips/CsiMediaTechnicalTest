﻿using CsiMediaTechnicalTest.DataAccess.Models;

namespace CsiMediaTechnicalTest.Service.Services
{
    public class SortOperationService(IRepository<SortOperation> repository) : ISortOperationService
    {
        public async Task SaveOperation(SortOperation operation)
        {
            await repository.AddAsync(operation);
            await repository.SaveChangesAsync();
        }

        public async Task<IEnumerable<SortOperation>> GetSortOperations()
        {
            return await repository.GetAllAsync();
        }
    }
}