﻿using CsiMediaTechnicalTest.DataAccess.Models;

namespace CsiMediaTechnicalTest.Service.Services;

public interface ISortOperationService
{
    Task SaveOperation(SortOperation operation);
    Task<IEnumerable<SortOperation>> GetSortOperations();
}