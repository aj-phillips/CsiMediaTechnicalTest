﻿using CsiMediaTechnicalTest.DataAccess.Models;
using CsiMediaTechnicalTest.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CsiMediaTechnicalTest.DataAccess.IntegrationTests;

[TestFixture]
public class SortOperationRepositoryTests
{
    private SortOperationRepository _repository = null!;
    private AppDbContext _context = null!;

    [SetUp]
    public void Setup()
    {
        var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase")
            .Options;

        _context = new AppDbContext(options);

        _repository = new SortOperationRepository(_context);
    }

    [TearDown]
    public async Task TearDown()
    {
        foreach (var operation in _context.SortOperations)
        {
            _context.SortOperations.Remove(operation);
        }
        await _context.SaveChangesAsync();
    }

    [Test]
    public async Task AddAsync_AddsSortOperation_Successfully()
    {
        var operation = new SortOperation
        {
            SortedNumbers = "1, 2, 3",
            SortOrder = "Ascending",
            SortTime = DateTime.Now,
            Duration = TimeSpan.FromSeconds(5)
        };

        await _repository.AddAsync(operation);
        await _repository.SaveChangesAsync();

        Assert.That(operation.Id, Is.EqualTo(_context.SortOperations.Single().Id));
    }
    
    [Test]
    public async Task GetAllAsync_GetsAllSortOperations_Successfully()
    {
        var operations = new List<SortOperation>
        {
            new SortOperation
            {
                SortedNumbers = "4, 5, 6",
                SortOrder = "Descending",
                SortTime = DateTime.Now,
                Duration = TimeSpan.FromSeconds(10)
            },
            new SortOperation
            {
                SortedNumbers = "7, 8, 9",
                SortOrder = "Ascending",
                SortTime = DateTime.Now,
                Duration = TimeSpan.FromSeconds(15)
            },
        };

        foreach (var operation in operations)
        {
            _context.SortOperations.Add(operation);
        }

        await _context.SaveChangesAsync();

        var result = await _repository.GetAllAsync();

        Assert.That(result.Count(), Is.EqualTo(operations.Count));
    }
    
    [Test]
    public async Task GetByIdAsync_GetsSortOperationById_Successfully()
    {
        var operation = new SortOperation
        {
            SortedNumbers = "1, 2, 3",
            SortOrder = "Ascending",
            SortTime = DateTime.Now,
            Duration = TimeSpan.FromSeconds(5)
        };
        
        _context.SortOperations.Add(operation);
        await _context.SaveChangesAsync();

        var result = await _repository.GetByIdAsync(operation.Id);

        Assert.That(result, Is.EqualTo(operation));
    }

    [Test]
    public async Task Update_UpdatesSortOperation_Successfully()
    {
        var operation = new SortOperation
        {
            SortedNumbers = "1, 2, 3",
            SortOrder = "Ascending",
            SortTime = DateTime.Now,
            Duration = TimeSpan.FromSeconds(5)
        };
        
        _context.SortOperations.Add(operation);
        await _context.SaveChangesAsync();

        operation.Duration = TimeSpan.FromSeconds(10);
        
        _repository.Update(operation);
        await _repository.SaveChangesAsync();

        var operationInDb = _context.SortOperations.Single();
        Assert.That(operationInDb.Duration, Is.EqualTo(TimeSpan.FromSeconds(10)));
    }

    [Test]
    public async Task Delete_DeletesSortOperation_Successfully()
    {
        var operation = new SortOperation
        {
            SortedNumbers = "1, 2, 3",
            SortOrder = "Ascending",
            SortTime = DateTime.Now,
            Duration = TimeSpan.FromSeconds(5)
        };
        
        _context.SortOperations.Add(operation);
        await _context.SaveChangesAsync();

        _repository.Delete(operation);
        await _repository.SaveChangesAsync();

        Assert.That(_context.SortOperations.Count(), Is.EqualTo(0));
    }
}